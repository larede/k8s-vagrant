#!/bin/bash
set -e
function set_user(){
    pg_md5 -m --config-file="/opt/bitnami/pgpool/conf/pgpool.conf" -u "${1}" "${2}"
}
function get_env(){

DB_ALFRESCO_USER=alfresco
DB_ALFRESCO_PASSWORD=alfresco
DB_KEYCLOAK_USER=keycloak
DB_KEYCLOAK_PASSWORD=keycloak
DB_TNT_USER=tnt
DB_TNT_PASSWORD=tnt
DB_ADM_USER=adm
DB_ADM_PASSWORD=adm
DB_AMA_USER=ama
DB_AMA_PASSWORD=ama
DB_CAN_USER=can
DB_CAN_PASSWORD=can
DB_ENT_USER=ent
DB_ENT_PASSWORD=ent
DB_FUN_USER=fun
DB_FUN_PASSWORD=fun
DB_GAS_USER=gas
DB_GAS_PASSWORD=gas
DB_GDOC_USER=gdoc
DB_GDOC_PASSWORD=gdoc
DB_OLD_USER=old
DB_OLD_PASSWORD=old
DB_SAD_USER=sad
DB_SAD_PASSWORD=sad
DB_SID_USER=sid
DB_SID_PASSWORD=sid
DB_SNC_USER=snc
DB_SNC_PASSWORD=snc
DB_STA_USER=sta
DB_STA_PASSWORD=sta
DB_MOR_USER=mor
DB_MOR_PASSWORD=mor
DB_FEI_USER=fei
DB_FEI_PASSWORD=fei

users=($(compgen -A variable | grep "DB_.*_USER"))
passwords=($(compgen -A variable | grep "DB_.*_PASSWORD"))
i=0
for iter in "${users[@]}"
do
    set_user ${!users[$i]} ${!passwords[$i]}
    i=$i+1    
done
}
get_env