#!/bin/bash
set -e
function create_database()
{
echo "Creating database role: $2"

PGPASSWORD=${POSTGRES_PASSWORD} psql --username ${POSTGRES_USER} <<-EOSQL
CREATE USER ${2} WITH CREATEDB PASSWORD '${3}';
EOSQL

echo "Creating database: $1"

PGPASSWORD=${POSTGRES_PASSWORD} psql --username ${POSTGRES_USER} <<EOSQL
CREATE DATABASE ${1} OWNER ${2};
EOSQL
}

function get_env()
{
    DB_ALFRESCO_NAME=alfresco
    DB_ALFRESCO_USER=alfresco
    DB_ALFRESCO_PASSWORD=alfresco
    DB_KEYCLOAK_NAME=keycloak
    DB_KEYCLOAK_USER=keycloak
    DB_KEYCLOAK_PASSWORD=keycloak
    DB_TNT_NAME=tnt
    DB_TNT_USER=tnt
    DB_TNT_PASSWORD=tnt
    DB_ADM_NAME=adm
    DB_ADM_USER=adm
    DB_ADM_PASSWORD=adm
    DB_AMA_NAME=ama
    DB_AMA_USER=ama
    DB_AMA_PASSWORD=ama
    DB_CAN_NAME=can
    DB_CAN_USER=can
    DB_CAN_PASSWORD=can
    DB_ENT_NAME=ent
    DB_ENT_USER=ent
    DB_ENT_PASSWORD=ent
    DB_FUN_NAME=fun
    DB_FUN_USER=fun
    DB_FUN_PASSWORD=fun
    DB_GAS_NAME=gas
    DB_GAS_USER=gas
    DB_GAS_PASSWORD=gas
    DB_GDOC_NAME=gdoc
    DB_GDOC_USER=gdoc
    DB_GDOC_PASSWORD=gdoc
    DB_OLD_NAME=old
    DB_OLD_USER=old
    DB_OLD_PASSWORD=old
    DB_SAD_NAME=sad
    DB_SAD_USER=sad
    DB_SAD_PASSWORD=sad
    DB_SID_NAME=sid
    DB_SID_USER=sid
    DB_SID_PASSWORD=sid
    DB_SID_SCHEMAS=rvp,aut,sgv
    DB_SNC_NAME=snc
    DB_SNC_USER=snc
    DB_SNC_PASSWORD=snc
    DB_SNC_SCHEMAS=docprev,regcont,fundisp,patrimonio
    DB_SNC_EXTENSIONS=btree_gist
    DB_STA_NAME=sta
    DB_STA_USER=sta
    DB_STA_PASSWORD=sta
    DB_MOR_NAME=mor
    DB_MOR_USER=mor
    DB_MOR_PASSWORD=mor
    DB_FEI_NAME=fei
    DB_FEI_USER=fei
    DB_FEI_PASSWORD=fei

    databases=($(compgen -A variable | grep "DB_.*_NAME"))
    users=($(compgen -A variable | grep "DB_.*_USER"))
    passwords=($(compgen -A variable | grep "DB_.*_PASSWORD"))
    i=0
    for iter in "${databases[@]}"
    do
        db=${!databases[$i]}
        user=${!users[$i]}
        create_database ${db} ${user} ${!passwords[$i]}

        DB_LABEL=$(echo ${databases[$i]} | grep -o -P '(?<=DB_).*(?=_NAME)');
        # DB Schemas
        DB_SCHEMAS="DB_"$DB_LABEL"_SCHEMAS";
        if [[ -v $DB_SCHEMAS ]]; then
            for schema in $(echo ${!DB_SCHEMAS} | tr ',' ' '); do
                echo "Creating schema: $schema"
                PGPASSWORD=${POSTGRES_PASSWORD} psql --username ${POSTGRES_USER} -d ${db} <<EOSQL
CREATE SCHEMA IF NOT EXISTS $schema;
GRANT ALL ON SCHEMA $schema to "${user}";
EOSQL
            done
        fi

        # DB Extensions
        DB_EXTENSIONS="DB_"$DB_LABEL"_EXTENSIONS";
        if [[ -v $DB_EXTENSIONS ]]; then
            for extension in $(echo ${!DB_EXTENSIONS} | tr ',' ' '); do
                echo "Creating extension: $extension"
                PGPASSWORD=${POSTGRES_PASSWORD} psql --username ${POSTGRES_USER} -d ${db} <<EOSQL
CREATE EXTENSION IF NOT EXISTS $extension;
EOSQL
            done
        fi
        i=$i+1
    done
}

get_env