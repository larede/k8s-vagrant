data "helm_repository" "bitnami" {
  name = "bitnami"
  url  = "https://charts.bitnami.com/bitnami"
}

resource "helm_release" "db" {
  name       = "db"
  repository = data.helm_repository.bitnami.metadata[0].name
  chart      = "postgresql"
  version    = "8.9.0"
  timeout    = 600

  values = [
    "${file("../keycloak/postgres-values.yaml")}"
  ]
}