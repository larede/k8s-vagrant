data "helm_repository" "codecentric" {
  name = "codecentric"
  url  = "https://codecentric.github.io/helm-charts"
}

resource "helm_release" "keycloak" {
  name       = "keycloak"
  repository = data.helm_repository.codecentric.metadata[0].name
  chart      = "keycloak"
  version    = "7.7.0"
  timeout    = 600

  values = [
    "${file("../keycloak/keycloak-values.yml")}"
  ]
}