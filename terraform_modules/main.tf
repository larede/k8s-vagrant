module "keycloak" {
    source = "./keycloak"
}

module "db" {
    source = "./postgres"
}