# README #

### What is this repository for? ###

* k8s cluster on local machine

for more info <https://kubernetes.io/blog/2019/03/15/kubernetes-setup-using-ansible-and-vagrant/>

### Requirements ###

* Ubuntu 18.04
* Virtualbox
* Vagrant

### How do I get set up? ###

* vagrant up
* vagrant ssh k8s-master
* vagrant ssh node-1
* vagrant halt
* vagrant destroy

### Configure Local Kubectl to Access Remote Kubernetes Cluster ###

  https://kubernetes.io/docs/tasks/tools/install-kubectl/

* scp -r vagrant@192.168.50.10:/home/vagrant/.kube .

* cp -r .kube $HOME/

or 

* export KUBECONFIG=$PWD/.kube/config

   ( local terminal session )

* kubectl get pods --all-namespaces

### Kubernetes Web Dashboard UI ###

* kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.0.0/aio/deploy/recommended.yaml
* kubectl apply -f k8s-dashboard-admin-user.yaml

* SECRET=$(kubectl get sa admin-user -n kube-system -o jsonpath='{.secrets[0].name}')
* echo $(kubectl get secret $SECRET -n kube-system -o jsonpath='{.data.token}' | base64 --decode)

  ( copy token to clipboard )

* kubectl proxy

<http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/>

### Instal MetalLB v0.9.3 with Layer 2 Configuration on Kubernetes ###

  If you’re using kube-proxy in IPVS mode, since Kubernetes v1.14.2 you have to enable strict ARP mode.

* kubectl edit configmap -n kube-system kube-proxy

  and set:
    ipvs:
      strictARP: true

  You can also add this configuration snippet to your kubeadm-config, just append it with --- after the main configuration

  You can use Kubernetes Dashbord to edit the configmap.

* kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.9.3/manifests/namespace.yaml
* kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.9.3/manifests/metallb.yaml
* kubectl create secret generic -n metallb-system memberlist --from-literal=secretkey="$(openssl rand -base64 128)"
* kubectl apply -f metallb.yaml

  for more info <https://metallb.universe.tf/>

### Download and Install Istio on Kubernetes ###

* curl -L https://istio.io/downloadIstio | sh -
* cd istio-*
* export PATH=$PWD/bin:$PATH
* istioctl manifest apply --set profile=default

  for more info about istio profiles visit <https://istio.io/docs/setup/additional-setup/config-profiles/>

  check prometheus installation on localhost

* kubectl -n istio-system port-forward $(kubectl -n istio-system get pod -l app=prometheus -o jsonpath='{.items[0].metadata.name}') 9090:9090

  check EXTERNAL-IP on istio-ingressgateway service

* kubectl get services -n istio-system istio-ingressgateway

### Install Postgres and Keycloak from helm chart ###

  The package manager for Kubernetes Helm is the best way to find, share, and use software built for Kubernetes.
  https://helm.sh/docs/intro/install/

* helm repo add bitnami https://charts.bitnami.com/bitnami
* helm install db -f keycloak/postgres-values.yaml bitnami/postgresql

  for more info https://github.com/bitnami/charts/tree/master/bitnami/postgresql

* kubectl get pods

* helm repo add codecentric https://codecentric.github.io/helm-charts
* helm install keycloak -f keycloak/keycloak-values.yml codecentric/keycloak
* kubectl apply -f keycloak/keycloak-gateway.yml

  for more info https://github.com/codecentric/helm-charts/tree/master/charts/keycloak

* kubectl get pods
* kubectl get virtualservice
* kubectl get gateway

  check installation <http://192.168.50.10/auth>
  admin / admin

### Uninstall all services and gateways ###

* helm ls --all --short | xargs -L1 helm delete
* kubectl get virtualservice -o=jsonpath="{.items[0].metadata.name}" | xargs -L1 kubectl delete virtualservice
* kubectl get gateway -o=jsonpath="{.items[0].metadata.name}" | xargs -L1 kubectl delete gateway

## Terraform to install Postgres and Keycloak ###

  https://learn.hashicorp.com/terraform/getting-started/install.html

* cd terraform_modules
* terraform init
* terraform plan
* terraform apply
* cd ..
* kubectl apply -f keycloak/keycloak-gateway.yml

* kubectl get pods
* kubectl get virtualservice
* kubectl get gateway

  check installation <http://192.168.50.10/auth>
  admin / admin

### Uninstall all services and gateways ###

* helm ls --all --short | xargs -L1 helm delete
* kubectl get virtualservice -o=jsonpath="{.items[0].metadata.name}" | xargs -L1 kubectl delete virtualservice
* kubectl get gateway -o=jsonpath="{.items[0].metadata.name}" | xargs -L1 kubectl delete gateway

## Longhorn and PostgresHA ###

  https://longhorn.io/docs/0.8.0/install/install-with-helm/

* git clone https://github.com/longhorn/longhorn
* kubectl create namespace longhorn-system
* helm install longhorn ./longhorn/chart/ --namespace longhorn-system --set persistence.defaultClassReplicaCount=1
* kubectl get pods -A

  kubectl -n longhorn-system port-forward $(kubectl -n longhorn-system get pod -l app=longhorn-ui -o jsonpath='{.items[0].metadata.name}') 8000:8000

  https://github.com/bitnami/charts/tree/master/bitnami/postgresql-ha

* kubectl create configmap pgpool-init --from-file postgres-ha/pgpool-init.sh
* kubectl create configmap database-init --from-file postgres-ha/database-init.sh
* helm repo add bitnami https://charts.bitnami.com/bitnami
* helm install database bitnami/postgresql-ha -f postgres-ha/values.yml
* kubectl get pods -A

  Check connection_life_time on pgpool changed:
  
    kubectl exec --stdin --tty $(kubectl -n default get pod -l app.kubernetes.io/component=pgpool -o jsonpath='{.items[0].metadata.name}') -- /bin/bash
    
      cat /opt/bitnami/pgpool/user_config/pgpool.conf

  Check max_connections and shared_buffers on postgres changed:

    kubectl exec --stdin --tty $(kubectl -n default get pod -l app.kubernetes.io/component=postgresql -o jsonpath='{.items[0].metadata.name}') -- /bin/bash
    
      psql -U postgres
      
        SHOW max_connections;
        
        SHOW shared_buffers;

  Check with pgadmin:

    kubectl port-forward svc/database-postgresql-ha-pgpool 5435:5432

  Check with java client:

  * helm install keycloak -f keycloak/keycloak-values.yml codecentric/keycloak --set keycloak.persistence.dbHost=database-postgresql-ha-pgpool.default.svc.cluster.local

  * kubectl -n default port-forward keycloak-0 8080:8080